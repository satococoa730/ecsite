<?php
session_start();
session_regenerate_id(true);
require_once '../common/function.php';// for user-defined function
error_reporting(E_ALL);
 ini_set('display_errors',1);
try {
    $db = dbConnect();
    $sql = "SELECT * FROM products";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchall(PDO::FETCH_ASSOC);

} catch (PDOException $e) {
    echo "接続失敗:" .$e->getMessage(). "\n";
} finally {
    $db = null;
}
?>

<!DOCTYPE html>
<html>
    <head>
      <meta charset="UTF-8">
      <title>商品一覧</title>
    </head>
    <body>
    <h2>商品一覧ページ</h2>
<?php
    $db=dbConnect();
    $sql="SELECT *  FROM products WHERE id=(SELECT MAX(id) FROM products)";
    $stmt=$db->prepare($sql);
    $stmt->execute();
    $new= $stmt->fetch(PDO::FETCH_ASSOC);
 
    $id=$new['id'];
?>
    <form action="new_product.php" method="POST"><input type="submit" value="新着情報！"><input type="hidden" name="new" value="<?php echo $id;?>"></form>
	<?php echo $new['name']; ?><br>

    <table border="1">
      <tr>
        <th>商品名</th>
        <th>値段</th>
        <th style="width:300px;">紹介文</th>
        <th>詳細</th>
      </tr>
      <?php
//var_dump($result);
      foreach($result as $row){
      ?>
      <tr>
        <td><?php echo $row['name']; ?></td>
        <td><?php echo '¥'.$row['price']; ?></td>
        <td><?php echo $row['introduction']; ?></td>
        <td><form action="product_detail.php" method="POST"><button type="submit" name="id" value="<?php echo $row['id']; ?>">詳しく見る</button></form></td>
      </tr>
      <?php
      }
      ?>
    </table>
    <a href="rank.php">ランキング</a><br>
    <a href="../cart/cart.php">カートを見る</a><br />
    <a href="../auth/logout.php">ログアウト</a>
    </body>
</html>
