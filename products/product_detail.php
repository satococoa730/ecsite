<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
session_start();
session_regenerate_id(true);
require_once '../common/function.php';// for user-defined function

// カートに追加機能
if (isset($_POST['add_cart'])) {
    // カートが空の場合の処理
    if (empty($_SESSION['cart'])) {
        $_SESSION['cart'] = array();
        // 商品IDと数量をセッションに代入
        $ary = array('id' => $_POST['pro_id'], 'amount' => $_POST['amount']);
        $_SESSION['cart'][] = $ary;
        header('Location: product_list.php');
    } else {
        // カートに同じ商品が入っているか確認
        foreach ($_SESSION['cart'] as $key => $val) {
            if ($val['id'] == $_POST['pro_id']) {
                $_SESSION['cart'][$key]['amount'] += $_POST['amount'];
                header('Location: product_list.php');
                exit();
            }
        }
        $ary = array('id' => $_POST['pro_id'], 'amount' => $_POST['amount']);
        $_SESSION['cart'][] = $ary;
        header('Location: product_list.php');
    }
}
if(isset($_POST['kutikomi_reg'])){
   $session = sanitize($_SESSION);	
   $user_id = intval($session['user_id']); 

   if($user_id==null){
      header('Location: ../auth/login.php');
       }else if(isset($user_id)){
      ?>
  
<?php header("Location: kutikomi.php?pro_id={$_POST['pro_id']}");
      exit();
    }
}
if(isset($_POST['delete_kuti'])){
   $session = sanitize($_SESSION);
   $user_id = intval($session['user_id']);
   
   if($user_id==null){
      header('Location: ../auth/login.php');
       }else if(isset($user_id)){
      ?>
      
<?php header('Location: delete_kutikomi.php');
      exit();
    }
 }
$db = dbConnect();
$sql = "SELECT * FROM products WHERE id = {$_POST['id']}";

$stmt = $db->prepare($sql);
$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);
//echo '<pre>';
//var_dump($result);
//echo '</pre>';
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>商品詳細</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
    <h1>商品詳細</h1>
    <p>商品名：<?php echo $result['name']; ?></p>
    <p>お値段：¥<?php echo $result['price']; ?></p>
    <p>紹介文</P>
    <?php echo $result['introduction']; ?><br />
    <?php
    if ($result['image']) {
        echo '<img src="../img/'.$result['image'].'">';
    } else {
        echo '<img src="../img/no_image.png">';
    }
    ?>
    <form action="" method="POST" style="margin:20px;">
        <input type="hidden" name="pro_id" value="<?php echo $result['id']; ?>">
        <span>数量：</span><input type="number" name="amount" value="1" min="1">
        <input type="submit" name="add_cart" value="カートに入れる">
    </form>
    <a href="../products/product_list.php">戻る</a><br />

    <form action="" method="POST"> 

	<input type="hidden" name="pro_id" value="<?php echo $result['id']; ?>">
	<input type="submit" name="kutikomi_reg" value="口コミを登録する">
    </form>
    <form method="post" action="">
       <input type="submit" name="delete_kuti" value="口コミを削除する">
    </form> 

<?php
$id=$result['id'];
$session = sanitize($_SESSION);
$user_id = intval($session['user_id']);
if($user_id==null){
  echo "";
}
elseif (isset($user_id)) {
?>
<button>good</button>	
<script type="text/javascript">	
//var param='<?php echo $id;?>';		
$('button').click(function(){    
  $.ajax({
    type:"POST",
    url:"ajax.php",
    data:{
      "pro_id":param

	},
   success:function(msg){
alert(msg);   
	   $('button').hide();
   },
   error:function(XMLHttpRequest,textStatus,errorThrown){
      alert(textStatus);
   }
  });

});
</script>
<?php } ?>

<?php
$db = dbConnect();
$sql = "SELECT * FROM favorites WHERE product_id = {$id}";
//var_dump($sql);
$stmt = $db->prepare($sql);
$stmt->execute();
$result = $stmt->fetchall(PDO::FETCH_ASSOC);
$favorites=0;
foreach($result as $row){
$favorites+=$row['value'];
}
//var_dump($row['value']);
echo "いいね数".$favorites;
?>


  <h2>口コミ欄</h2>
   
      
    <?php

	try {
        $db = dbConnect();
	$sql = "SELECT * FROM kutikomi where product_id = :product_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':product_id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchall(PDO::FETCH_ASSOC);

        if (! $result) {
            echo "";
	}elseif(isset($result)){
          foreach($result as $row){
      ?>
      
        <?php echo "ニックネーム".$row['nikuname']; ?>
        <?php echo "口コミ".$row['kutikomi']; ?>
      <br>  
     <?php
	   }
	  }
    } catch (PDOException $e) {
        echo "接続失敗:" .$e->getMessage(). "\n";
    } finally {
        $db = null;
    }
    ?>
</body>
</html>
